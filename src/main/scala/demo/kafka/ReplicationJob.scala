package demo.kafka

import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.streaming.api.functions.sink.SinkFunction
import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.scala.{StreamExecutionEnvironment, createTypeInformation}

object ReplicationJob {
  private var count = 0

  def apply(env: StreamExecutionEnvironment, source: SourceFunction[String], sink: SinkFunction[Message], params: ParameterTool): Unit = {
    env.addSource(source)
      .map((m: String) => {
        count = count + 1
        Message(m, count)
      })
      .addSink(sink)

    env.execute("replication")
  }
}
