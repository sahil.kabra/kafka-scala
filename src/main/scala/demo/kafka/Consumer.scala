package demo.kafka

import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.{FlinkKafkaConsumer, FlinkKafkaProducer}

object Consumer {
  def main(args: Array[String]): Unit = {
    val params = ParameterTool.fromArgs(args)

    val env = StreamExecutionEnvironment.getExecutionEnvironment
    val source = new FlinkKafkaConsumer[Message](params.get("topic"), new MessageSchema(), params.getProperties)
    val sink = new FlinkKafkaProducer[Message](params.get("bootstrap.servers"), params.get("out.topic"), new MessageSchema())

    ConsumerJob(env, source, sink, params)
  }
}
