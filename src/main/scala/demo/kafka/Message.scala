package demo.kafka

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.apache.flink.api.common.serialization.{DeserializationSchema, SerializationSchema}
import org.apache.flink.api.common.typeinfo.TypeInformation
import org.apache.flink.api.java.typeutils.TypeExtractor

case class Message(message: String, count: Int)

class MessageSchema extends SerializationSchema[Message] with DeserializationSchema[Message] {
  override def serialize(message: Message): Array[Byte] = MessageSchema.mapper.writeValueAsBytes(message)

  override def deserialize(message: Array[Byte]): Message = MessageSchema.mapper.readValue(message, classOf[Message])

  override def isEndOfStream(nextElement: Message): Boolean = false

  override def getProducedType: TypeInformation[Message] = TypeExtractor.getForClass(classOf[Message])
}

object MessageSchema {
  private val mapper = new ObjectMapper()
  mapper.registerModule(DefaultScalaModule)
  mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
}
