package demo.kafka

import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.streaming.api.scala.{StreamExecutionEnvironment, createTypeInformation}
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer
import org.apache.flink.streaming.connectors.wikiedits.WikipediaEditsSource

object Producer {
  def main(args: Array[String]): Unit = {
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment

    env.addSource(new WikipediaEditsSource())
      .map(_.getUser)
      .addSink(new FlinkKafkaProducer[String]("localhost:9092", "test", new SimpleStringSchema()))

    env.execute()
  }
}
