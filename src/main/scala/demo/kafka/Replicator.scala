package demo.kafka

import org.apache.flink.api.common.serialization.SimpleStringSchema
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.{FlinkKafkaConsumer, FlinkKafkaProducer}

object Replicator {
  def main(args: Array[String]): Unit = {
    val params = ParameterTool.fromArgs(args)

    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    val source = new FlinkKafkaConsumer[String](params.get("topic"), new SimpleStringSchema(), params.getProperties)
    val sink = new FlinkKafkaProducer[Message](params.get("bootstrap.servers"), params.get("replication.topic"), new MessageSchema())

    ReplicationJob(env, source, sink, params)
  }
}

