package demo.kafka

import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.streaming.api.functions.sink.SinkFunction
import org.apache.flink.streaming.api.functions.source.SourceFunction
import org.apache.flink.streaming.api.scala.{StreamExecutionEnvironment, createTypeInformation}

object ConsumerJob {

  def apply(env: StreamExecutionEnvironment, source: SourceFunction[Message], sink: SinkFunction[Message], params: ParameterTool): Unit = {
    env.addSource(source).addSink(sink)

    env.execute("consumer")
  }
}
